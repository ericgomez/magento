<?php

namespace Marcgento\BasicModule\Controller\Adminhtml\Subscription;

use  Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Delete extends Action
{
    /**
     * @var \Marcgento\BasicModule\Model\Subscription
     */
    protected $modelSubscription;

    /**
     * @param Context $context
     * @param \Marcgento\BasicModule\Model\Subscription $subscriptionFactory
     */
    public function __construct(
        Context $context,
        \Marcgento\BasicModule\Model\Subscription $subscriptionFactory
    ) {
        $this->modelSubscription = $subscriptionFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Marcgento_BasicModule::subscription_delete');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('subscription_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->modelSubscription->load($id);
                $model->delete();

                $this->messageManager->addSuccess(__('The subscription has been deleted.'));

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());

                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a subscription to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
