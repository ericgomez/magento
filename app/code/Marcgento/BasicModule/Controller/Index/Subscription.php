<?php

namespace Marcgento\BasicModule\Controller\Index;

class Subscription extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        $subscription = $this->_objectManager->create('Marcgento\BasicModule\Model\Subscription');

        $subscription->setName('Eric');
        $subscription->setLastName('Gomez');
        $subscription->setEmail('eric.gomez.sanchez@gmail.com');
        $subscription->setMessage('This is a test message');


        $subscription->save();
        $this->getResponse()->setBody('Subscription saved');
    }
}
