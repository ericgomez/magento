<?php

namespace Marcgento\BasicModule\Model\ResourceModel\Subscription;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Marcgento\BasicModule\Model\Subscription', 'Marcgento\BasicModule\Model\ResourceModel\Subscription');
	}

}
